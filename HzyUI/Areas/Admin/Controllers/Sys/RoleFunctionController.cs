﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//
using Aop;
using DbFrame;
using DbFrame.Class;
using Common;
using BLL;
using Models;

namespace HzyUI.Areas.Admin.Controllers.Sys
{
    public class RoleFunctionController : BaseController
    {
        // 角色功能
        // GET: /ManageSys/RoleFunction/
        protected override void Init()
        {
            this.MenuID = "Z-140";
        }

        Sys_RoleBL _Sys_RoleBL = new Sys_RoleBL();
        Sys_MenuBL _Sys_MenuBL = new Sys_MenuBL();

        #region  基本操作，增删改查

        /// <summary>
        /// 获取角色菜单功能
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetRoleMenuFunctionTree(string roleid)
        {
            return this.Success(new
            {
                status = 1,
                value = _Sys_MenuBL.GetRoleMenuFunctionTree(roleid)
            });
        }

        /// <summary>
        /// 保存角色功能
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Save(string rows, string roleid)
        {
            _Sys_RoleBL.SaveFunction(rows, roleid);
            return this.Success();
        }
        #endregion 基本操作，增删改查
    }
}
