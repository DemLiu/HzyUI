﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aop
{
    //引用
    using System.Web;
    using System.Web.Mvc;
    using DbFrame;
    using DbFrame.Class;
    using Common;

    /// <summary>
    /// 实体 验证
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class AopCheckEntityAttribute : ActionFilterAttribute
    {
        private DBContext db = new DBContext();
        private string[] ParamName { get; set; }

        public AopCheckEntityAttribute(string[] _ParamName)
        {
            this.ParamName = _ParamName;
        }

        /// <summary>
        /// 每次请求Action之前发生，，在行为方法执行前执行
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            foreach (var item in ParamName)
            {
                var _Value = (EntityClass)filterContext.ActionParameters[item];
                if (_Value != null)
                {
                    if (!db.CheckModel(_Value))
                    {
                        throw new MessageBox(db.ErrorMessge);
                    }
                }
            }


        }

    }
}