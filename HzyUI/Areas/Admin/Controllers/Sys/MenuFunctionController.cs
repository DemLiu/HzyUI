﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//
using Aop;
using DbFrame;
using DbFrame.Class;
using Common;
using BLL;
using Models;
using System.Collections;

namespace HzyUI.Areas.Admin.Controllers.Sys
{
    public class MenuFunctionController : BaseController
    {
        // 菜单功能
        // GET: /ManageSys/MenuFunction/
        protected override void Init()
        {
            this.MenuID = "Z-130";
        }

        Sys_MenuBL _Sys_MenuBL = new Sys_MenuBL();

        #region  查询数据列表
        /// <summary>
        /// 获取列表数据
        /// </summary>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        [NonAction]
        public override Sys_PagingEntity GetPagingEntity(Hashtable query, int page = 1, int rows = 20)
        {
            //获取列表
            return _Sys_MenuBL.GetDataSource(query, page, rows);
        }
        #endregion  查询数据列表

        /// <summary>
        /// 获取菜单和功能树
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetMenuAndFunctionTree()
        {
            return Success(new
            {
                status = 1,
                value = _Sys_MenuBL.GetMenuAndFunctionTree()
            });
        }

        #region  基本操作，增删改查
        /// <summary>
        /// 保存
        /// </summary>
        /// <returns></returns>
        [AopCheckEntity(new string[] { "model" })]
        [HttpPost]
        public ActionResult Save(Sys_MenuM model, string Function_ID)
        {
            this.KeyID = _Sys_MenuBL.Save(model, Function_ID);
            return this.Success(new
            {
                status = 1,
                ID = this.KeyID
            });
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(string ID)
        {
            _Sys_MenuBL.Delete(ID);
            return this.Success();
        }

        /// <summary>
        /// 查询根据ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Find(string ID)
        {
            return this.Success(_Sys_MenuBL.Find(ID.ToGuid()));
        }

        /// <summary>
        /// 保存菜单功能
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveMenuFunction(string nodes)
        {
            _Sys_MenuBL.SaveMenuFunction(nodes);
            return this.Success();
        }
        #endregion  基本操作，增删改查

    }
}
