using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//
using Aop;
using DbFrame;
using DbFrame.Class;
using Common;
using BLL;
using Models;

namespace HzyUI.Areas.Admin.Controllers.Sys
{
    public class CreateCodeController : BaseController
    {
        // 根据表 生成 代码
        // GET: /SysManage/CreateCode/
        protected override void Init()
        {
            this.MenuID = "Z-160";
        }

        Sys_CreateCodeBL _Sys_CreateCodeBL = new Sys_CreateCodeBL();

        /// <summary>
        /// 获取数据库中所有的表和字段
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetDatabaseAllTable()
        {
            return this.Success(new { status = 1, value = _Sys_CreateCodeBL.GetDatabaseAllTable() });
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Save(FormCollection fc)
        {
            var Type = fc["ClassType"];
            var Url = (fc["Url"] == null ? Server.MapPath("/Content/CreateFile") : fc["Url"]);
            var Str = fc["Str"];
            var Table = fc["Table"];
            var isall = fc["isall"].ToBool();
            var template = Server.MapPath("/Content/Template/");

            if (Type == "Model")
            {
                Url = (Url + "\\Model");
                template = template + "Model\\Model.txt";
                Str = string.IsNullOrEmpty(Str.ToStr()) ? "M" : Str.ToStr();
            }
            else if (Type == "BLL")
            {
                Url = Url + "\\BLL";
                template = template + "Bll\\BLL.txt";
                Str = string.IsNullOrEmpty(Str.ToStr()) ? "BL" : Str.ToStr();
            }
            else if (Type == "DAL")
            {
                Url = Url + "\\DAL";
                template = template + "DAL\\DAL.txt";
                Str = string.IsNullOrEmpty(Str.ToStr()) ? "DA" : Str.ToStr();
            }

            if (System.IO.Directory.Exists(Url + "\\"))
            {
                var dir = new System.IO.DirectoryInfo(Url + "\\");
                var fileinfo = dir.GetFileSystemInfos();  //返回目录中所有文件和子目录
                foreach (var i in fileinfo)
                {
                    if (i is System.IO.DirectoryInfo)            //判断是否文件夹
                    {
                        var subdir = new System.IO.DirectoryInfo(i.FullName);
                        subdir.Delete(true);          //删除子目录和文件
                    }
                    else
                    {
                        System.IO.File.Delete(i.FullName);      //删除指定文件
                    }
                }
                //System.IO.Directory.Delete(Url + "\\");
            }
            System.IO.Directory.CreateDirectory(Url);

            if (!System.IO.File.Exists(template))
                throw new MessageBox("模板文件不存在");

            var Content = System.IO.File.ReadAllText(template);

            if (isall)
            {
                var list = _Sys_CreateCodeBL.GetAllTable();
                foreach (var item in list)
                {
                    Table = item["TABLE_NAME"] == null ? "" : item["TABLE_NAME"].ToString();
                    _Sys_CreateCodeBL.CreateFileLogic(Content, Table, Str, Type, Url);
                }
            }
            else
            {
                if (string.IsNullOrEmpty(Table))
                    throw new MessageBox("请选择表");
                _Sys_CreateCodeBL.CreateFileLogic(Content, Table, Str, Type, Url);
            }

            return this.Success();
        }



    }
}
