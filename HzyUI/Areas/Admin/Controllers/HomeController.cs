﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//
using Aop;
using DbFrame;
using DbFrame.Class;
using Common;
using BLL;
using Models;

namespace HzyUI.Areas.Admin.Controllers
{
    public class HomeController : BaseController
    {
        //
        // GET: /Admin/Home/
        protected override void Init()
        {
            base.Init();
            this.IsExecutePowerLogic = false;
        }

        Sys_MenuBL _Sys_MenuBL = new Sys_MenuBL();

        public override ActionResult Index()
        {
            ViewData["MenuHtml"] = _Sys_MenuBL.GetSysMenu();
            return View(Account);
        }

        /// <summary>
        /// 首页
        /// </summary>
        /// <returns></returns>
        public ActionResult Main()
        {
            return View();
        }

    }
}
