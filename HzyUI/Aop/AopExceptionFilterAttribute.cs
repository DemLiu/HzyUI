using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Aop
{
    using Common;

    /// <summary>
    /// 程序异常过滤器
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class AopExceptionFilterAttribute : HandleErrorAttribute
    {
        /// <summary>
        /// 是否 执行
        /// </summary>
        private bool _IsExecute { get; set; }
        public AopExceptionFilterAttribute(bool IsExecute)
        {
            _IsExecute = IsExecute;
        }

        public override void OnException(ExceptionContext filterContext)
        {
            if (_IsExecute)
                ExceptionWeb(filterContext);
            base.OnException(filterContext);
        }

        /// <summary>
        /// 后端异常处理
        /// </summary>
        private void ExceptionWeb(ExceptionContext filterContext)
        {
            //判断是否是自定义异常类型
            if (filterContext.Exception is MessageBox)
            {
                if (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
                    //返回错误信息
                    filterContext.Result = new JsonResult() { Data = MessageBox.errorModel };
                else
                {
                    var errorModel = new ErrorModel(filterContext.Exception.Message);
                    var sb = new StringBuilder();
                    sb.Append("<script src=\"/Resource/HzyUI/lib/jquery/jquery-2.1.4.min.js\"></script>");
                    sb.Append("<script src=\"/Resource/Admin/lib/layer-v3.1.1/layer/layer.js\"></script>");
                    sb.Append("<script src=\"/Resource/Admin/js/admin.js\"></script>");
                    sb.Append("<script type='text/javascript'>");
                    sb.Append("$(function(){ admin.alert('" + errorModel.msg.Trim().Replace("'", "“").Replace("\"", "”") + "', '警告'); });");
                    sb.Append("</script>");
                    filterContext.Result = new ContentResult() { Content = sb.ToString(), ContentType = "text/html;charset=utf-8;" };
                }
                filterContext.HttpContext.Response.StatusCode = 200;
            }
            else
            {
                ErrorWrite(filterContext);
                ErrorModel em = new ErrorModel(filterContext.Exception.Message);
                if (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
                {
                    //返回错误信息
                    filterContext.Result = new JsonResult() { Data = em };
                    filterContext.HttpContext.Response.StatusCode = 200;
                }
                else
                {
                    filterContext.Result = new ViewResult() { ViewName = AppConfig.ErrorPageUrl, ViewData = new ViewDataDictionary<ErrorModel>(em) };
                }
            }
            //表示异常已处理
            filterContext.ExceptionHandled = true;
        }


        /// <summary>
        /// 写入错误
        /// </summary>
        /// <param name="filterContext"></param>
        public void ErrorWrite(ExceptionContext filterContext)
        {
            string errorinfo = string.Empty;
            string errorsource = string.Empty;
            string errortrace = string.Empty;

            errorinfo = "异常信息: " + filterContext.Exception.Message;
            errorsource = "错误源:" + filterContext.Exception.Source;
            errortrace = "堆栈信息:" + filterContext.Exception.StackTrace;
            filterContext.HttpContext.Server.ClearError();

            string line = "-----------------------------------------------------";

            string log = line + "\r\n" + filterContext.HttpContext.Request.UserHostAddress.ToString() + "\r\n" + errorinfo + "\r\n" + errorsource + "\r\n" + errortrace + "\r\n";
            LogHelper.WriteLog(log);
            LogHelper.WriteLog("应用程序错误");
        }


    }
}
