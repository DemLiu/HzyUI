﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//
using Aop;
using DbFrame;
using DbFrame.Class;
using Common;
using BLL;
using Models;
using System.Collections;

namespace HzyUI.Areas.Admin.Controllers.Sys
{
    public class FunctionController : BaseController
    {
        // 功能管理
        // GET: /SysManage/Function/
        protected override void Init()
        {
            this.MenuID = "Z-120";
        }

        Sys_FunctionBL _Sys_FunctionBL = new Sys_FunctionBL();

        #region  查询数据列表
        /// <summary>
        /// 获取列表数据
        /// </summary>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        [NonAction]
        public override Sys_PagingEntity GetPagingEntity(Hashtable query, int page = 1, int rows = 20)
        {
            //获取列表
            return _Sys_FunctionBL.GetDataSource(query, page, rows);
        }
        #endregion  查询数据列表

        #region  基本操作，增删改查
        /// <summary>
        /// 保存
        /// </summary>
        /// <returns></returns>
        [AopCheckEntity(new string[] { "model" })]
        [HttpPost]
        public ActionResult Save(Sys_FunctionM model)
        {
            this.KeyID = _Sys_FunctionBL.Save(model);
            return this.Success(new { status = 1, ID = KeyID });
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(string ID)
        {
            _Sys_FunctionBL.Delete(ID);
            return this.Success();
        }

        /// <summary>
        /// 查询根据ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Find(string ID)
        {
            return this.Success(_Sys_FunctionBL.Find(ID.ToGuid()));
        }

        #endregion  基本操作，增删改查

    }
}
