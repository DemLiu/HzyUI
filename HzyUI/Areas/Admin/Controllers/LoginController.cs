﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//
using Aop;
using Models;
using DbFrame;
using DbFrame.Class;
using Common;
using Common.ValidateHelper;

namespace HzyUI.Areas.Admin.Controllers
{
    [AopActionFilter(false)]
    public class LoginController : BaseController
    {
        //
        // GET: /Admin/Login/

        protected override void Init()
        {
            base.Init();
            this.IsExecutePowerLogic = false;
        }

        Sys_UserM _Sys_UserM = new Sys_UserM();
        Sys_RoleM _Sys_RoleM = new Sys_RoleM();
        Sys_UserRoleM _Sys_UserRoleM = new Sys_UserRoleM();


        public override ActionResult Index()
        {
            Tools.SetSession("Account", new Sys_AccountM());
            return View();
        }

        /// <summary>
        /// 验证
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Checked(string uName, string uPwd, string loginCode)
        {
            if (string.IsNullOrEmpty(uName))
                throw new MessageBox("请输入用户名");
            if (string.IsNullOrEmpty(uPwd))
                throw new MessageBox("请输入密码");
            if (string.IsNullOrEmpty(loginCode))
                throw new MessageBox("请输入验证码");
            _Sys_UserM = db.Find<Sys_UserM>(w => w.User_LoginName == uName);

            if (_Sys_UserM.User_ID.ToGuid() == Guid.Empty)
                throw new MessageBox("用户不存在");
            if (_Sys_UserM.User_Pwd.ToStr().Trim() != uPwd)//Tools.MD5Encrypt(userpwd)))//
                throw new MessageBox("密码错误");
            string code = Tools.GetCookie("loginCode");
            if (string.IsNullOrEmpty(code))
                throw new MessageBox("验证码失效");
            if (!code.ToLower().Equals(loginCode.ToLower()))
                throw new MessageBox("验证码不正确");

            _Sys_UserRoleM = db.Find<Sys_UserRoleM>(w => w.UserRole_UserID == _Sys_UserM.User_ID);
            _Sys_RoleM = db.Find<Sys_RoleM>(w => w.Role_ID == _Sys_UserRoleM.UserRole_RoleID);

            var accountM = new Sys_AccountM();
            accountM.RoleID = _Sys_RoleM.Role_ID.ToGuid();
            accountM.UserID = _Sys_UserM.User_ID.ToGuid();
            accountM.UserName = _Sys_UserM.User_Name;
            //如果是超级管理员 帐户
            accountM.IsSuperManage = _Sys_RoleM.Role_ID == AppConfig.Admin_RoleID.ToGuid();

            Tools.SetSession("Account", accountM);
            return this.Success(new
            {
                status = 1,
                jumpurl = AppConfig.HomePageUrl
            });
        }

        /// <summary>
        /// 获取验证码
        /// </summary>
        /// <returns></returns>
        public ActionResult GetYZM()
        {
            ValidateCodeHelper vch = new ValidateCodeHelper();
            string code = vch.GetRandomNumberString(4);
            Tools.SetCookie("loginCode", code, 2);
            return File(vch.CreateImage(code), "image/jpeg");
        }

        /// <summary>
        /// 退出登录
        /// </summary>
        /// <returns></returns>
        public ActionResult Out()
        {
            return RedirectToAction("Index");
        }


    }
}
