﻿using System.Web.Mvc;

namespace HzyUI.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Admin_default",
                "Admin/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { 
                "HzyUI.Areas."+AreaName+".Controllers",
                "HzyUI.Areas."+AreaName+".Controllers.Sys",
                "HzyUI.Areas."+AreaName+".Controllers.Base"
                }
            );
        }
    }
}
